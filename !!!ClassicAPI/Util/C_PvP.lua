local _, Private = ...

local IsInInstance = IsInInstance
local GetMapInfo = GetMapInfo

local C_PvP = C_PvP or {}

local MAX_BATTLEGOUND_UNIT = {
	AlteracValley 		= 40,
	ArathiBasin 		= 15,
	NetherstormArena 	= 15,
	WarsongGulch 		= 10,
	IsleofConquest 		= 40,
	StrandoftheAncients = 15,
	LakeWintergrasp 	= 40,
}

function C_PvP.IsPvPMap()
	local Active, Type = IsInInstance()
	if ( Active ) then
		return Type == "pvp" or Type == "arena"
	end
end

function GetMaxUnitNumberBattleground()
	return MAX_BATTLEGOUND_UNIT[GetMapInfo()]
end

C_PvP.IsRatedBattleground = Private.False
C_PvP.IsWarModeDesired = Private.False

-- Global
_G.C_PvP = C_PvP