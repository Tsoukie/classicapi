local _, Private = ...

local ErrorDialog = error

--[[ INTERNAL ]]

-- Texture Path
Private.TEXTURE_PATH = "Interface\\AddOns\\!!!ClassicAPI\\Texture\\"

-- Errors
function Private.Error(Err)
	ErrorDialog(not Err and "Error!" or Err)
end

-- Common Functions
function Private.Void()
	-- Send it to the nether.
end

function Private.True()
	return true
end

function Private.False()
	return false
end

-- Scan Tooltip
local Tooltip = CreateFrame("GameTooltip", "__CAPIScanTooltip")
Tooltip:SetOwner(UIParent, "ANCHOR_NONE")
Tooltip:AddFontStrings(Tooltip:CreateFontString("$parentTextLeft1", nil, "GameTooltipText"), Tooltip:CreateFontString("$parentTextRight1", nil, "GameTooltipText"))
Private.Tooltip = Tooltip

--[[function Private.Globalize(Namespace, Data)
	_G[Namespace] = Data
	for Name,Func in pairs(Data) do
		local Exist = _G[Name]
		if ( Exist ) then
			if ( Func ~= Exist ) then
				Name = "C_"..Name
			end
		else
			_G[Name] = Func
		end
	end
end]]

--[[ MISCELLANEOUS ]]

-- [LFD_ERROR_FIX] This is unrelated to ClassicAPI, but I'm a chill guy :)
local LFDQueueFrameRandomCooldownFrame_Update = LFDQueueFrameRandomCooldownFrame_Update
local Sub = string.sub

-- LFD Lua Error
LFDQueueFrameCooldownFrame:SetScript("OnEvent", function(Self, Event, Unit)
	if ( Event ~= "UNIT_AURA" or (Unit and (Unit == "player" or Sub(Unit, 1, 5) == "party")) ) then
		LFDQueueFrameRandomCooldownFrame_Update()
	end
end)