local C_GossipInfo = C_GossipInfo or {}

C_GossipInfo.GetText = GetGossipText
C_GossipInfo.GetNumActiveQuests = GetNumGossipActiveQuests
C_GossipInfo.GetNumAvailableQuests = GetNumGossipAvailableQuests

-- Global
_G.C_GossipInfo = C_GossipInfo