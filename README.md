# ClassicAPI for 3.3.5a client
__`Library to emulate aspects of the Classic API for the 3.3.5a client.`__

<img src="https://i.imgur.com/viZROsQ.jpeg" width="100%">

---

### ⚠ Note: Still in early development and many features will be missing!
### 📥 [Installation](#-installation-1)
### 📋 [Report Issue](https://gitlab.com/Tsoukie/classicapi/-/issues)
### 💬 [FAQ](#-faq-1)
### ❤️ [Support & Credit](#%EF%B8%8F-support-credit-1)

<!-- blank line -->
<br>
<!-- blank line -->

# 📥 Installation

1. Download Latest Release `[.zip, .gz, ...]`:
	- <a href="https://gitlab.com/Tsoukie/classicapi/-/releases/permalink/latest" target="_blank">`📥 ClassicAPI`</a>
2. Extract the downloaded compressed file _(eg. Right-Click -> Extract-All)_.
3. Navigate within extracted folder(s) looking for the following: `!!!ClassicAPI`.
4. Move folder(s) named `!!!ClassicAPI` to your `Interface\AddOns\` folder.
5. Re-launch game.

<!-- blank line -->
<br>
<!-- blank line -->


# 💬 FAQ

> What does this addon actually do?

It's simply an addon that stores/emulates missing functions, variables, events and systems to allow certain addons to run on the 3.3.5a client. It does not patch, or modify your client in any way.

> What is the motivation for this?

After backporting addons, I wanted to make the backporting process a bit simpler for future projects. This is _very_ much a work in progress. Many features will be missing and things won't work flawlessly. It should also be mentioned a lot of features are simply impossible to emulate/backport. Despite many limitations the goal is to get as much working as possible to alleviate time spent backporting.

> Can I run 3.3.5a addons alongside this?

Yes. Though some addons do have a small chance to conflict, especially if they're also backported. I'm actively working to avoid these types of issues as much as possible.

> Can I download and run any Classic addon?

No. Some very simple addons _may_ work, but more advanced ones likely still require changes to be completely compatible.

> I found a bug!

Please 📋 [report the issue](https://gitlab.com/Tsoukie/classicapi/-/issues) with as much detail as possible.

<!-- blank line -->
<br>
<!-- blank line -->


# ❤️ Support & Credit
 
If you wish to show some support you can do so [here](https://streamlabs.com/tsoukielol/tip). Tips are completely voluntary and aren't required to download my projects, however, they are _very_ much appreciated. They allow me to devote more time to creating things I truly enjoy. 💜

<!-- blank line -->
<br>
<!-- blank line -->
  
_This_ addon has been created and maintained by [Tsoukie](https://gitlab.com/Tsoukie/) and is **not** related or affiliated with any other version.